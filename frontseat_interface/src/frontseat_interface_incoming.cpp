/**
 * Copyright 2018-2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*********************************************
 * MANAGES UDP LINK TO FRONTSEAT COMPUTER
 *
 * Authors: Greg Okopal, 2018
 * Contact: okopal@uw.edu
 **********************************************/

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <iostream>
#include <sstream>

#include "remus_backseat_msgs/RemusString.h"
#include "ros/ros.h"
#include "std_msgs/String.h"

using boost::asio::ip::udp;

int main(int argc, char** argv) {
  // init ROS stuff
  ros::init(argc, argv, "frontseat_interface_incoming");
  ros::NodeHandle pnh("~");
  ros::NodeHandle nh;
  ros::Publisher frontseat_string_pub =
      nh.advertise<remus_backseat_msgs::RemusString>(
          "frontseat_string_to_parser", 10);
  ros::Rate loop_rate(100);

  // get parameters
  std::string server_ip;
  if (pnh.getParam("server_ip", server_ip)) {
    ROS_WARN_STREAM("setting server ip: " << server_ip);
  } else {
    ROS_FATAL("frontseat_interface_incoming requires param: server_ip");
    ros::shutdown();
  }
  int port;
  if (pnh.getParam("port", port)) {
    ROS_WARN_STREAM("setting port: " << port);
  } else {
    ROS_FATAL("frontseat_interface_incoming requires param: port");
    ros::shutdown();
  }

  // set up a UDP listener
  boost::asio::io_service io_service;
  udp::endpoint local_endpoint = boost::asio::ip::udp::endpoint(
      boost::asio::ip::address_v4::any(), boost::lexical_cast<int>(port));
  udp::socket socket(io_service);
  socket.open(udp::v4());
  boost::asio::socket_base::reuse_address option(true);
  socket.set_option(option);
  socket.bind(local_endpoint);
  boost::array<char, 1024> recv_buf;
  udp::endpoint sender_endpoint;

  // listen to the UDP socket and log any messages
  while (ros::ok()) {
    try {
      recv_buf.assign(0);
      size_t len =
          socket.receive_from(boost::asio::buffer(recv_buf), sender_endpoint);
      ROS_INFO_STREAM("[FRONTSEATIN] " << recv_buf.data());
    } catch (std::exception& e) {
      std::cerr << e.what() << std::endl;
      // NOTE(lindzey): Shouldn't this have a "continue;"??
    }

    // create a message with the string and publish it
    // QUESTION(lindzey): I'm not familiar with converting from a char array to
    // a string. Are there any values that don't convert? Or is std::string
    // basically an array of bytes, with no assumption of utf-8 or other
    // encoding?
    remus_backseat_msgs::RemusString msg;
    // Want to timestamp this data as close as possible to the time
    // at which it was received from the frontseat.
    msg.header.stamp = ros::Time::now();
    msg.data = recv_buf.data();
    frontseat_string_pub.publish(msg);
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}
