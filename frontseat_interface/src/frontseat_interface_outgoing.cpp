/**
 * Copyright 2018-2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*********************************************
 * MANAGES UDP LINK TO FRONTSEAT COMPUTER
 *
 * Authors: Greg Okopal, 2018
 * Contact: okopal@uw.edu
 **********************************************/
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <iostream>
#include <sstream>

#include "ros/ros.h"
#include "std_msgs/String.h"

using boost::asio::ip::udp;

class UDPStringSender {
 public:
  std::string server_ip;
  int port;

  boost::asio::io_service io_service;
  boost::asio::ip::udp::socket *socket;
  boost::asio::ip::udp::endpoint remote_endpoint;

  UDPStringSender(std::string server_ip_in, int port_in) {
    server_ip = std::string(server_ip_in);
    port = port_in;
    initSocket();
  }

  void initSocket() {
    std::cout << "making socket" << std::endl;
    socket = new boost::asio::ip::udp::socket(io_service);
    std::cout << "opening socket" << std::endl;
    socket->open(boost::asio::ip::udp::v4());
    std::cout << "creating endpoint" << std::endl;
    remote_endpoint = boost::asio::ip::udp::endpoint(
        boost::asio::ip::address::from_string(server_ip),
        boost::lexical_cast<int>(port));
  }

  void frontseatSendStringCallback(const std_msgs::String::ConstPtr &msg) {
    boost::system::error_code ignored_error;
    ROS_INFO_STREAM("[FRONTSEATOUT] " << msg->data);
    socket->send_to(boost::asio::buffer(msg->data, msg->data.length()),
                    remote_endpoint, 0, ignored_error);
  }
};

int main(int argc, char **argv) {
  // init ROS stuff
  ros::init(argc, argv, "frontseat_interface_outgoing");
  ros::NodeHandle pnh("~");
  ros::NodeHandle nh;

  std::string ip;
  int port;

  // get parameters
  if (pnh.getParam("server_ip", ip)) {
    ROS_WARN_STREAM("setting server ip: " << ip);
  } else {
    ROS_FATAL("frontseat_interface_outgoing requires param: server_ip");
    ros::shutdown();
  }

  if (pnh.getParam("port", port)) {
    ROS_WARN_STREAM("setting port: " << port);
  } else {
    ROS_FATAL("frontseat_interface_outgoing requires param: port");
    ros::shutdown();
  }

  // create the string sender
  UDPStringSender udpss(ip, port);

  ros::Subscriber sub =
      nh.subscribe("composer_string_to_frontseat", 1000,
                   &UDPStringSender::frontseatSendStringCallback, &udpss);
  ros::spin();

  return 0;
}
