/**
 * Copyright 2017-2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*********************************************
 * PARSES STRINGS FROM FRONT SEAT, PUBLISHES
 * DATA AS ROS MESSAGES
 *
 * Authors: Thomas Swanson, Greg Okopal  2017-2018
 * Contact: okopal@uw.edu
 **********************************************/

#ifndef MSG_PARSER_INCLUDE_MSG_PARSER_MESSAGEPARSER_H_
#define MSG_PARSER_INCLUDE_MSG_PARSER_MESSAGEPARSER_H_

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/function.hpp>
#include <boost/tokenizer.hpp>
#include <map>
#include <string>
#include <vector>

#include "apl_msgs/Ctd.h"
#include "remus_backseat_msgs/R2VHeartbeatRequest.h"
#include "remus_backseat_msgs/RemusString.h"
#include "remus_backseat_msgs/V2RAdcp.h"
#include "remus_backseat_msgs/V2RAnchorStatus.h"
#include "remus_backseat_msgs/V2RBattery.h"
#include "remus_backseat_msgs/V2RCommandAcknowledge.h"
#include "remus_backseat_msgs/V2RCtd.h"
#include "remus_backseat_msgs/V2RDusblRangeBearing.h"
#include "remus_backseat_msgs/V2RError.h"
#include "remus_backseat_msgs/V2RFixUpdatedByVehicle.h"
#include "remus_backseat_msgs/V2RFluorometer.h"
#include "remus_backseat_msgs/V2RImagenexForwardLookingAltimeter.h"
#include "remus_backseat_msgs/V2RModemDirectMonitoringInterface.h"
#include "remus_backseat_msgs/V2RModemMessage.h"
#include "remus_backseat_msgs/V2RNavigationUncertainty.h"
#include "remus_backseat_msgs/V2RRoute.h"
#include "remus_backseat_msgs/V2RSetStatus.h"
#include "remus_backseat_msgs/V2RSidescanNotification.h"
#include "remus_backseat_msgs/V2RState.h"
#include "remus_backseat_msgs/V2RTime.h"
#include "remus_backseat_msgs/V2RVersionInformation.h"
#include "remus_backseat_msgs/V2RVersionInformationRequest.h"
#include "ros/ros.h"

// for extracting status bits
#define STATUS_OVERRIDE_ENABLED 0x04
#define STATUS_OVERRIDE_ACTIVE 0x08

// Checks whether the input_string has a valid checksum
bool ChecksumIsValid(const std::string& input_string);

class MessageParser {
 public:
  MessageParser();
  ~MessageParser() = default;

 private:
  typedef boost::function<void(const ros::Time& msg_time,
                               std::vector<std::string> token_vec_in)>
      function_t;
  typedef std::map<std::string, function_t> functionMap_t;

  // map from Recon message indicator (e.g. "#A") to function used to parse it
  functionMap_t parserFunctionMap;

  // load all parameters from the param server
  void SetupParameters();
  // Set up all publishers
  void SetupPublishers();
  void SetupFunctionMap();

  // Callback for every string received; dispatches to message-specific
  // callback based on the message indicator character.
  // Discards any message with an invalid checksum.
  void ParserCallback(const remus_backseat_msgs::RemusString::ConstPtr& msg);

  // Message-specific callbacks that parse a Recon string into a ROS msg
  void V2RAdcpCallback(const ros::Time& msg_time,
                       std::vector<std::string> token_vec_in);
  void V2RAnchorStatusCallback(const ros::Time& msg_time,
                               std::vector<std::string> token_vec_in);
  void V2RBatteryCallback(const ros::Time& msg_time,
                          std::vector<std::string> token_vec_in);
  void V2RCommandAcknowledgeCallback(const ros::Time& msg_time,
                                     std::vector<std::string> token_vec_in);
  void V2RCtdCallback(const ros::Time& msg_time,
                      std::vector<std::string> token_vec_in);
  void V2RDusblRangeBearingCallback(const ros::Time& msg_time,
                                    std::vector<std::string> token_vec_in);
  void V2RErrorCallback(const ros::Time& msg_time,
                        std::vector<std::string> token_vec_in);
  void V2RFixUpdatedByVehicleCallback(const ros::Time& msg_time,
                                      std::vector<std::string> token_vec_in);
  void V2RFluorometerCallback(const ros::Time& msg_time,
                              std::vector<std::string> token_vec_in);
  void V2RImagenexForwardLookingAltimeterCallback(
      const ros::Time& msg_time, std::vector<std::string> token_vec_in);
  void V2RModemDirectMonitoringInterfaceCallback(
      const ros::Time& msg_time, std::vector<std::string> token_vec_in);
  void V2RModemMessageCallback(const ros::Time& msg_time,
                               std::vector<std::string> token_vec_in);
  void V2RNavigationUncertaintyCallback(const ros::Time& msg_time,
                                        std::vector<std::string> token_vec_in);
  void V2RRouteCallback(const ros::Time& msg_time,
                        std::vector<std::string> token_vec_in);
  void V2RSetStatusCallback(const ros::Time& msg_time,
                            std::vector<std::string> token_vec_in);
  void V2RSidescanNotificationCallback(const ros::Time& msg_time,
                                       std::vector<std::string> token_vec_in);
  void V2RStateCallback(const ros::Time& msg_time,
                        std::vector<std::string> token_vec_in);
  void V2RTimeCallback(const ros::Time& msg_time,
                       std::vector<std::string> token_vec_in);
  void V2RVersionInformationCallback(const ros::Time& msg_time,
                                     std::vector<std::string> token_vec_in);
  void V2RVersionInformationRequestCallback(
      const ros::Time& msg_time, std::vector<std::string> token_vec_in);

  void tokentLit(const ros::Time& msg_time,
                 std::vector<std::string> token_vec_in);
  void tokenhLit(const ros::Time& msg_time,
                 std::vector<std::string> token_vec_in);
  // Allows user to turn on/off publication of individual Recon messages.
  // Case-sensitive, because #A and #a are different messages.
  bool A_active, B_active, C_active, E_active, F_active, M_active, N_active,
      R_active, S_active, U_active, V_active, Z_active, a_active, c_active,
      f_active, h_active, i_active, k_active, l_active, m_active, s_active,
      t_active, v_active;

  const char delimiter = ',';
  const std::string kAdcp = "#A";
  const std::string kAnchorStatus = "#a";
  const std::string kBattery = "#B";
  const std::string kCommandAcknowledge = "#C";
  const std::string kCtd = "#c";
  const std::string kDusblRangeBearing = "#U";
  const std::string kError = "#E";
  const std::string kFixUpdatedByVehicle = "#f";
  const std::string kFluorometer = "#F";
  const std::string kImagenexForwardLookingAltimeter = "#i";
  const std::string kModemDirectMonitoringInterface = "#M";
  const std::string kModemMessage = "#m";
  const std::string kNavigationUncertainty = "#k";
  const std::string kRoute = "#R";
  const std::string kSetStatus = "#s";
  const std::string kSidescanNotification = "#N";
  const std::string kState = "#S";
  const std::string kTime = "#Z";
  const std::string kVersionInformation = "#V";
  const std::string kVersionInformationRequest = "#v";

  ros::NodeHandle nh_;

  // Publishers that do a one-for-one translation of Recon messages
  ros::Publisher V2RADCP_pub_;
  ros::Publisher V2RAnchorstatus_pub_;
  ros::Publisher V2RBatterydata_pub_;
  ros::Publisher V2RCommandAcknowledge_pub_;
  ros::Publisher V2RCTDdata_pub_;
  ros::Publisher V2RDUSBLrangebearing_pub_;
  ros::Publisher V2RErrormessage_pub_;
  ros::Publisher V2RFixupdatedbyvehicle_pub_;
  ros::Publisher V2RFluormeterdata_pub_;
  ros::Publisher V2RImagenexForwardlookingaltimeter_pub_;
  ros::Publisher V2RModemdirectmonitoringinterface_pub_;
  ros::Publisher V2RModemmessage_pub_;
  ros::Publisher V2RNavigationUncertaintymessage_pub_;
  ros::Publisher V2RRouteinformation_pub_;
  ros::Publisher V2RSetstatus_pub_;
  ros::Publisher V2RSidescannotifymessage_pub_;
  ros::Publisher V2RState_pub_;
  ros::Publisher V2RTimemessage_pub_;
  ros::Publisher V2RVersioninformation_pub_;
  ros::Publisher V2RVersioninformationrequest_pub_;

  // publishers for "standardized" messages
  ros::Publisher ctd_pub_;

  ros::Subscriber frontseat_sub_;
};

#endif  // MSG_PARSER_INCLUDE_MSG_PARSER_MESSAGEPARSER_H_
