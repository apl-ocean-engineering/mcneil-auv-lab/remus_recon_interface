## msg_parser

Subscribe to strings received from Recon over the UDP interface
(the frontseat_interface publishes these strings) and parse them
into the equivalent ROS messages.

msg_parser.cpp implements `main()`, while MessageParser.cpp implements all the callbacks that handle individual messages. Subscribing/publishing happens *outside* of the class.

NOTE: Not all messages are currently supported; it appears that an earlier version based on backseatUtility::parseString handled more of them, but has been deprecated in favor of using boost::tokenizer.

TODO: Check that all messages our remus is configured to publish are supported? I'm particularly interested in being able to see Fluormeter and Modem data.

TODO: consider unifying the nomenclature for messages, before using this operationally and enshrining them in our logs? (whether to append 'data', 'message'; camelcase vs not)


Implemented:
* #A -- V2RADCP
* #B -- V2RBatterydata
* #S -- V2RState
* #Z -- V2RTimemessage

* #c -- V2RCTDdata
* #f -- V2RFixupdatedbyvehicle
* #v -- R2VHeartbeatRequest

NYI: (messages that are published, but not filled in)
* #E -- V2RErrormessage
* #F -- V2RFluormeterdata
* #M -- V2RModemdirectmonitoringinterface
* #N -- V2RSidescannotifymessage
* #U -- V2RDUSBLrangebearing

* #a -- V2RAnchorstatus
* #i -- V2RImagenexForwardlookingaltimeter
* #k -- V2RNavigationUncertaintymessage
* #m -- V2RModemmessage
* #s -- V2RSetstatus


Entirely Unsupported:
* #C -- V2RCommandAcknowledge (msg created, empty not published)
* #R -- V2RRouteinformation
* #V -- V2RVersioninformation

* #h -- ???? callback is tokenhLit, no associated publisher. "acknowledges hover commands from the Remote"
* #t -- ???? callback is tokentLit, no associated publisher. "acknowledges the request to terminate the objective"
