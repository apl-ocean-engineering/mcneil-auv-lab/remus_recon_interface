/**
 * Copyright 2018-2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <gtest/gtest.h>

#include "msg_parser/MessageParser.h"

TEST(MessageParser, test_nominal) {
  // Example strings from the manual
  std::string F_msg("#F,G5,V0.000,*D7");
  std::string S_msg(
      "#S,T11:09:21.6,LAT33N14.9322,LON117W27.0600,D-0.02,DG0.00,A9.71,P-2.0,"
      "R3.0,TR0,TRG0,V0.00,H103.1,HR0.2,HG0.0,M00,*98");
  std::string C_msg("#C,Depth,Depth,2.0,*BA");

  for (const auto& msg : {F_msg, S_msg, C_msg}) {
    EXPECT_TRUE(ChecksumIsValid(msg));
  }
}

TEST(MessageParser, test_bad_checksum) {
  // Example strings from the manual, with checksums off-by-one
  std::string F_msg("#F,G5,V0.000,*D8");
  std::string S_msg(
      "#S,T11:09:21.6,LAT33N14.9322,LON117W27.0600,D-0.02,DG0.00,A9.71,P-2.0,"
      "R3.0,TR0,TRG0,V0.00,H103.1,HR0.2,HG0.0,M00,*99");
  std::string C_msg("#C,Depth,Depth,2.0,*CA");

  for (const auto& msg : {F_msg, S_msg, C_msg}) {
    EXPECT_FALSE(ChecksumIsValid(msg));
  }
}

TEST(MessageParser, test_invalid_checksum) {
  // Example strings from the manual, with checksums in invalid format
  std::string s1("#F,G5,V0.000");
  std::string s2("#F,G5,V0.000,");
  std::string s3("#F,G5,V0.000,*");
  std::string s4("#F,G5,V0.000,*D");
  std::string s5("#F,G5,V0.000,*D87");

  for (const auto& msg : {s1, s2, s3, s4, s5}) {
    EXPECT_FALSE(ChecksumIsValid(msg));
  }
}

// Run all tests
int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "test_msg_parser");
  return RUN_ALL_TESTS();
}
