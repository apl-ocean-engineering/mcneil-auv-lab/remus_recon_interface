/**
 * Copyright 2018-2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "msg_parser/MessageParser.h"

#include "remus_backseat_msgs/RemusString.h"
#include "ros/ros.h"

MessageParser::MessageParser() {
  nh_ = ros::NodeHandle();
  SetupParameters();
  SetupPublishers();
  SetupFunctionMap();
  // TODO: setup publishers!
  frontseat_sub_ = nh_.subscribe("frontseat_string_to_parser", 10,
                                 &MessageParser::ParserCallback, this);
}

void MessageParser::SetupFunctionMap() {
  // Set up parsers for each message type
  parserFunctionMap[kAdcp] =
      boost::bind(&MessageParser::V2RAdcpCallback, this, _1, _2);
  parserFunctionMap[kBattery] =
      boost::bind(&MessageParser::V2RBatteryCallback, this, _1, _2);
  parserFunctionMap[kState] =
      boost::bind(&MessageParser::V2RStateCallback, this, _1, _2);
  parserFunctionMap[kCommandAcknowledge] =
      boost::bind(&MessageParser::V2RCommandAcknowledgeCallback, this, _1, _2);
  parserFunctionMap[kFluorometer] =
      boost::bind(&MessageParser::V2RFluorometerCallback, this, _1, _2);
  parserFunctionMap[kVersionInformation] =
      boost::bind(&MessageParser::V2RVersionInformationCallback, this, _1, _2);
  parserFunctionMap[kRoute] =
      boost::bind(&MessageParser::V2RRouteCallback, this, _1, _2);
  parserFunctionMap[kTime] =
      boost::bind(&MessageParser::V2RTimeCallback, this, _1, _2);
  parserFunctionMap[kModemDirectMonitoringInterface] = boost::bind(
      &MessageParser::V2RModemDirectMonitoringInterfaceCallback, this, _1, _2);
  parserFunctionMap[kSidescanNotification] = boost::bind(
      &MessageParser::V2RSidescanNotificationCallback, this, _1, _2);
  parserFunctionMap[kDusblRangeBearing] =
      boost::bind(&MessageParser::V2RDusblRangeBearingCallback, this, _1, _2);
  parserFunctionMap[kError] =
      boost::bind(&MessageParser::V2RErrorCallback, this, _1, _2);
  parserFunctionMap[kCtd] =
      boost::bind(&MessageParser::V2RCtdCallback, this, _1, _2);
  parserFunctionMap[kVersionInformationRequest] = boost::bind(
      &MessageParser::V2RVersionInformationRequestCallback, this, _1, _2);
  parserFunctionMap[kAnchorStatus] =
      boost::bind(&MessageParser::V2RAnchorStatusCallback, this, _1, _2);
  parserFunctionMap[kSetStatus] =
      boost::bind(&MessageParser::V2RSetStatusCallback, this, _1, _2);
  parserFunctionMap[kFixUpdatedByVehicle] =
      boost::bind(&MessageParser::V2RFixUpdatedByVehicleCallback, this, _1, _2);
  parserFunctionMap[kModemMessage] =
      boost::bind(&MessageParser::V2RModemMessageCallback, this, _1, _2);
  parserFunctionMap[kNavigationUncertainty] = boost::bind(
      &MessageParser::V2RNavigationUncertaintyCallback, this, _1, _2);
  parserFunctionMap[kImagenexForwardLookingAltimeter] = boost::bind(
      &MessageParser::V2RImagenexForwardLookingAltimeterCallback, this, _1, _2);
}

void MessageParser::SetupPublishers() {
  // Initialize publishers for each message type if they are active
  if (A_active) {
    V2RADCP_pub_ = nh_.advertise<remus_backseat_msgs::V2RAdcp>("V2RAdcp", 1000);
  }

  if (B_active) {
    V2RBatterydata_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RBattery>("V2RBattery", 1000);
  }

  if (S_active) {
    V2RState_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RState>("V2RState", 1000);
  }

  if (C_active) {
    V2RCommandAcknowledge_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RCommandAcknowledge>(
            "V2RCommandAcknowledge", 1000);
  }

  if (F_active) {
    V2RFluormeterdata_pub_ = nh_.advertise<remus_backseat_msgs::V2RFluorometer>(
        "V2RFluorometer", 1000);
  }

  if (V_active) {
    // V2RVersioninformation_pub_ =
    // nh_.advertise<msg_composer::R2VHeartbeatRequest>("R2VHeartbeatRequest",
    // 1000);
  }

  if (R_active) {
    V2RRouteinformation_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RRoute>("V2RRoute", 1000);
  }

  if (Z_active) {
    V2RTimemessage_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RTime>("V2RTime", 1000);
  }

  if (M_active) {
    V2RModemdirectmonitoringinterface_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RModemDirectMonitoringInterface>(
            "V2RModemDirectMonitoringInterface", 1000);
  }

  if (N_active) {
    V2RSidescannotifymessage_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RSidescanNotification>(
            "V2RSidescanNotification", 1000);
  }

  if (U_active) {
    V2RDUSBLrangebearing_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RDusblRangeBearing>(
            "V2RDusblRangeBearing", 1000);
  }

  if (E_active) {
    V2RErrormessage_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RError>("V2RError", 1000);
  }

  if (c_active) {
    V2RCTDdata_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RCtd>("V2RCtd", 1000);
    ctd_pub_ = nh_.advertise<apl_msgs::Ctd>("ctd", 1000);
  }

  if (v_active) {
    V2RVersioninformationrequest_pub_ =
        nh_.advertise<remus_backseat_msgs::R2VHeartbeatRequest>(
            "R2VHeartbeatRequest", 1000);
  }

  if (a_active) {
    V2RAnchorstatus_pub_ = nh_.advertise<remus_backseat_msgs::V2RAnchorStatus>(
        "V2RAnchorStatus", 1000);
  }

  if (s_active) {
    V2RSetstatus_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RSetStatus>("V2RSetStatus", 1000);
  }

  if (f_active) {
    V2RFixupdatedbyvehicle_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RFixUpdatedByVehicle>(
            "V2RFixUpdatedByVehicle", 1000);
  }

  if (m_active) {
    V2RModemmessage_pub_ = nh_.advertise<remus_backseat_msgs::V2RModemMessage>(
        "V2RModemMessage", 1000);
  }

  if (k_active) {
    V2RNavigationUncertaintymessage_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RNavigationUncertainty>(
            "V2RNavigationUncertainty", 1000);
  }

  if (i_active) {
    V2RImagenexForwardlookingaltimeter_pub_ =
        nh_.advertise<remus_backseat_msgs::V2RImagenexForwardLookingAltimeter>(
            "V2RImagenexForwardLookingAltimeter", 1000);
  }
}

void MessageParser::SetupParameters() {
  ros::param::get("/activeMsg/A", A_active);
  ros::param::get("/activeMsg/B", B_active);
  ros::param::get("/activeMsg/S", S_active);
  ros::param::get("/activeMsg/C", C_active);
  ros::param::get("/activeMsg/F", F_active);
  ros::param::get("/activeMsg/V", V_active);
  ros::param::get("/activeMsg/R", R_active);
  ros::param::get("/activeMsg/Z", Z_active);
  ros::param::get("/activeMsg/M", M_active);
  ros::param::get("/activeMsg/N", N_active);
  // The U message isn't documented in the RECON protocol PDF that I have.
  // It seems like it's for the Digital Ultra Short Baseline unit that
  // Hydroid uses for line capture / line release docking.
  // Do we get these messages?
  ros::param::get("/activeMsg/U", U_active);
  ros::param::get("/activeMsg/E", E_active);
  ros::param::get("/activeMsg/c", c_active);
  ros::param::get("/activeMsg/v", v_active);
  ros::param::get("/activeMsg/a", a_active);
  ros::param::get("/activeMsg/s", s_active);
  ros::param::get("/activeMsg/f", f_active);
  ros::param::get("/activeMsg/m", m_active);
  ros::param::get("/activeMsg/k", k_active);
  ros::param::get("/activeMsg/i", i_active);
  ros::param::get("/activeMsg/l", l_active);
  ros::param::get("/activeMsg/t", t_active);
  ros::param::get("/activeMsg/h", h_active);

  // QUESTION(lindzey): Is this trying to hard-code defaults,
  //    or is it because the others aren't implemented, or something else?
  V_active = false;
  S_active = true;  // State is always active
  v_active = true;  // Heartbeat requests are always active
  A_active = true;  // ADCP
  f_active = true;
  c_active = true;
  Z_active = true;
  B_active = true;
}

bool ChecksumIsValid(const std::string& input_string) {
  int asterisk_index = input_string.find_last_of("*");
  if (asterisk_index <= input_string.length()) {
    asterisk_index += 1;
    int sum = 0;
    for (unsigned int ii = 0; ii < asterisk_index; ii++) {
      sum += input_string[ii];
    }
    int msg_checksum =
        strtoul(input_string.substr(asterisk_index).c_str(), NULL, 16);
    return ((sum & 0x0FF) == msg_checksum);
  } else {
    return false;
  }
}

void MessageParser::ParserCallback(
    const remus_backseat_msgs::RemusString::ConstPtr& msg) {
  if (ChecksumIsValid(msg->data)) {
    std::vector<std::string> token_vec;
    boost::char_separator<char> sep(",");
    boost::tokenizer<boost::char_separator<char> > tokens(msg->data, sep);
    for (const std::string& t : tokens) {
      token_vec.push_back(t);
    }
    if (token_vec.size() > 0) {
      // QUESTION(lindzey): Shouldn't we check that token_vec[0] is in
      //     parserFunctionMap?
      // TODO(lindzey): I'd like to write automated tests that exercise
      //     the ROS interfaces with valid and invalid data ...
      parserFunctionMap[token_vec[0]](msg->header.stamp, token_vec);
    }
  } else {
    ROS_ERROR_STREAM(
        "Discarding message due to invalid checksum: " << msg->data);
  }
}

void MessageParser::V2RAdcpCallback(const ros::Time& msg_time,
                                    std::vector<std::string> token_vec) {
  if (A_active) {
    remus_backseat_msgs::V2RAdcp msg;
    msg.header.stamp = msg_time;
    msg.forward_velocity = atof(token_vec[1].substr(1).c_str());
    msg.starboard_velocity = atof(token_vec[2].substr(1).c_str());
    msg.vertical_velocity = atof(token_vec[3].substr(1).c_str());
    msg.altitude = atof(token_vec[4].substr(1).c_str());
    msg.heading = atof(token_vec[5].substr(1).c_str());
    msg.forward_current = atof(token_vec[6].substr(2).c_str());
    msg.starboard_current = atof(token_vec[7].substr(2).c_str());
    msg.vertical_current = atof(token_vec[8].substr(2).c_str());
    if (token_vec[9] == "U") {
      msg.data_is_up_looking = true;
    } else {
      msg.data_is_up_looking = false;
    }
    V2RADCP_pub_.publish(msg);
  } else {
    ROS_INFO("msg #A is inactive");
  }
}

void MessageParser::V2RCtdCallback(const ros::Time& msg_time,
                                   std::vector<std::string> token_vec) {
  if (c_active) {
    remus_backseat_msgs::V2RCtd msg;
    msg.header.stamp = msg_time;
    msg.temperature = atof(token_vec[1].substr(1).c_str());
    msg.conductivity = atof(token_vec[2].substr(1).c_str());
    msg.salinity = atof(token_vec[3].substr(1).c_str());
    msg.depth = atof(token_vec[4].substr(1).c_str());
    msg.sound_speed = atof(token_vec[5].substr(1).c_str());
    V2RCTDdata_pub_.publish(msg);

    apl_msgs::Ctd apl_msg;
    apl_msg.header.stamp = msg_time;
    // Recon uses mS/cm; ROS uses S/m
    apl_msg.conductivity = msg.conductivity / 10;
    // both use degrees C
    apl_msg.temperature = msg.temperature;
    // ROS uses pressure in Pascals; Recon has already done the conversion (?!)
    apl_msg.pressure = apl_msgs::Ctd::CTD_NO_DATA;
    // both use parts per thousand
    apl_msg.salinity = msg.salinity;
    // both use m/s
    apl_msg.sound_speed = msg.sound_speed;
    ctd_pub_.publish(apl_msg);

  } else {
    ROS_INFO("msg #c is inactive");
  }
}
void MessageParser::V2RVersionInformationRequestCallback(
    const ros::Time& msg_time, std::vector<std::string> token_vec) {
  std::cout << "Info Request" << std::endl;
  if (v_active) {
    remus_backseat_msgs::R2VHeartbeatRequest msg;
    msg.header.stamp = msg_time;
    V2RVersioninformationrequest_pub_.publish(msg);
  } else {
    ROS_INFO("msg #v is inactive");
  }
}

void MessageParser::V2RAnchorStatusCallback(
    const ros::Time& msg_time, std::vector<std::string> token_vec) {
  if (a_active) {
    remus_backseat_msgs::V2RAnchorStatus msg;
    msg.header.stamp = msg_time;
    /*msg.stowedState =
    backseatUtility::parseString(stringToParse,"A","S");
    msg.anchoredState =
    backseatUtility::parseString(stringToParse,"S","F");
    msg.bladderState =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"F","P").c_str());
    std::string compare = backseatUtility::parseString(stringToParse,"P","V");
    msg.pumpState = (compare=="Off")?0:(compare=="On")? 1:0;
    compare = backseatUtility::parseString(stringToParse,"V","*");
    msg.valveState = (compare=="Closed")?0:(compare=="Open")? 1:0;*/
    V2RAnchorstatus_pub_.publish(msg);
  } else {
    ROS_INFO("msg #a is inactive");
  }
}

void MessageParser::V2RSetStatusCallback(const ros::Time& msg_time,
                                         std::vector<std::string> token_vec) {
  if (s_active) {
    remus_backseat_msgs::V2RSetStatus msg;
    msg.header.stamp = msg_time;
    /*msg.param = backseatUtility::parseString(stringToParse,"","");
    msg.paramVal =
    atoi(backseatUtility::parseString(stringToParse,"","").c_str());*/
    V2RSetstatus_pub_.publish(msg);
  } else {
    ROS_INFO("msg #s is inactive");
  }
}

void MessageParser::V2RFixUpdatedByVehicleCallback(
    const ros::Time& msg_time, std::vector<std::string> token_vec) {
  if (f_active) {
    remus_backseat_msgs::V2RFixUpdatedByVehicle msg;
    msg.header.stamp = msg_time;
    msg.type = token_vec[1];
    msg.position_string = token_vec[2];
    V2RFixupdatedbyvehicle_pub_.publish(msg);
  } else {
    ROS_INFO("msg #f is inactive");
  }
}

void MessageParser::V2RModemMessageCallback(
    const ros::Time& msg_time, std::vector<std::string> token_vec) {
  if (m_active) {
    remus_backseat_msgs::V2RModemMessage msg;
    msg.header.stamp = msg_time;
    /*msg.sourceAdress =
    backseatUtility::parseString(stringToParse,"","");
    msg.destinationAdress =
    atoi(backseatUtility::parseString(stringToParse,"","").c_str());
    msg.vehicleAdress =
    atoi(backseatUtility::parseString(stringToParse,"","").c_str());
    msg.modemMessage =
    backseatUtility::parseString(stringToParse,"","");*/
    V2RModemmessage_pub_.publish(msg);
  } else {
    ROS_INFO("msg #m is inactive");
  }
}

void MessageParser::V2RNavigationUncertaintyCallback(
    const ros::Time& msg_time, std::vector<std::string> token_vec) {
  if (k_active) {
    remus_backseat_msgs::V2RNavigationUncertainty msg;
    msg.header.stamp = msg_time;
    /*msg.dataSource =
    backseatUtility::parseString(stringToParse,"","");
    msg.navError =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"","").c_str());
    msg.distTravel =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"","").c_str());
    msg.percentError =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"","").c_str());*/
    V2RNavigationUncertaintymessage_pub_.publish(msg);
  } else {
    ROS_INFO("msg #k is inactive");
  }
}

void MessageParser::V2RImagenexForwardLookingAltimeterCallback(
    const ros::Time& msg_time, std::vector<std::string> token_vec) {
  if (i_active) {
    remus_backseat_msgs::V2RImagenexForwardLookingAltimeter msg;
    msg.header.stamp = msg_time;
    /*msg.range =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"","").c_str());
    msg.altitude =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"","").c_str());
    msg.lookDownAngle =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"","").c_str());
    msg.rangeSetting =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"","").c_str());
    msg.pitchInDegree =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"","").c_str());*/
    V2RImagenexForwardlookingaltimeter_pub_.publish(msg);
  } else {
    ROS_INFO("msg #i is inactive");
  }
}

void MessageParser::tokentLit(const ros::Time& msg_time,
                              std::vector<std::string> token_vec) {
  if (t_active) {
  } else {
    ROS_INFO("msg #t is inactive");
  }
}

void MessageParser::tokenhLit(const ros::Time& msg_time,
                              std::vector<std::string> token_vec) {
  if (h_active) {
  } else {
    ROS_INFO("msg #h is inactive");
  }
}

void MessageParser::V2RCommandAcknowledgeCallback(
    const ros::Time& msg_time, std::vector<std::string> token_vec) {
  if (C_active) {
    remus_backseat_msgs::V2RCommandAcknowledge msg;
    msg.header.stamp = msg_time;
  } else {
    ROS_INFO("msg #C is inactive");
  }
}

void MessageParser::V2RBatteryCallback(const ros::Time& msg_time,
                                       std::vector<std::string> token_vec) {
  if (B_active) {
    remus_backseat_msgs::V2RBattery msg;
    msg.header.stamp = msg_time;
    msg.capacity = atof(token_vec[1].substr(1).c_str());
    msg.available = atof(token_vec[2].substr(1).c_str());
    msg.percentage_available = atof(token_vec[3].substr(1).c_str());
    V2RBatterydata_pub_.publish(msg);
  } else {
    ROS_INFO("msg #B is inactive");
  }
}

void MessageParser::V2RStateCallback(const ros::Time& msg_time,
                                     std::vector<std::string> token_vec) {
  if (S_active) {
    remus_backseat_msgs::V2RState msg;
    msg.header.stamp = msg_time;
    msg.mission_time.sec = atoi(token_vec[1].substr(1, 2).c_str()) * 3600 +
                           atoi(token_vec[1].substr(4, 2).c_str()) * 60 +
                           atoi(token_vec[1].substr(7, 2).c_str());
    msg.mission_time.nsec =
        atoi(token_vec[1].substr(10, 2).c_str()) * 100000000;

    int deg_ind = token_vec[2].find_first_not_of("LATON");
    int NS_ind = token_vec[2].find_first_of("NS");
    int latitude_sign = 1;
    if (token_vec[2].substr(NS_ind, 1) == "S") {
      latitude_sign = -1;
    }
    msg.latitude =
        latitude_sign *
        (atof(token_vec[2].substr(deg_ind, NS_ind - deg_ind).c_str()) +
         atof(token_vec[2].substr(NS_ind + 1).c_str()) / 60.0);

    deg_ind = token_vec[3].find_first_not_of("LATON");
    int EW_ind = token_vec[3].find_first_of("EW");
    int longitude_sign = 1;
    if (token_vec[3].substr(EW_ind, 1) == "W") {
      longitude_sign = -1;
    }
    msg.longitude =
        longitude_sign *
        (atof(token_vec[3].substr(deg_ind, EW_ind - deg_ind).c_str()) +
         atof(token_vec[3].substr(EW_ind + 1).c_str()) / 60.0);

    msg.depth = atof(token_vec[4].substr(1).c_str());
    msg.depth_goal = atof(token_vec[5].substr(2).c_str());
    msg.altitude = atof(token_vec[6].substr(1).c_str());
    msg.pitch = atof(token_vec[7].substr(1).c_str());
    msg.roll = atof(token_vec[8].substr(1).c_str());
    msg.thruster_rpm = atof(token_vec[9].substr(2).c_str());
    msg.thruster_rpm_goal = atof(token_vec[10].substr(3).c_str());
    msg.velocity = atof(token_vec[11].substr(1).c_str());
    msg.heading = atof(token_vec[12].substr(1).c_str());
    msg.heading_rate = atof(token_vec[13].substr(2).c_str());
    msg.heading_goal = atof(token_vec[14].substr(2).c_str());
    msg.mode = strtoul(token_vec[15].substr(1).c_str(), NULL, 16);
    msg.override_enabled = STATUS_OVERRIDE_ENABLED & msg.mode;
    msg.override_active = STATUS_OVERRIDE_ACTIVE & msg.mode;
    msg.leg = atoi(token_vec[16].substr(1).c_str());
    V2RState_pub_.publish(msg);
  } else {
    ROS_INFO("msg #S is inactive");
  }
}

void MessageParser::V2RFluorometerCallback(const ros::Time& msg_time,
                                           std::vector<std::string> token_vec) {
  if (F_active) {
    remus_backseat_msgs::V2RFluorometer msg;
    msg.header.stamp = msg_time;
    // QUESTION(lindzey): Will this be a problem? Do we expect to have
    //     Fluoremeter data in the ROS logs, or will Craig be pulling it from
    //     the Remus logs?
    /*msg.gain =
    atoi(backseatUtility::parseString(stringToParse,"G","V").c_str());
    msg.v =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"V","A").c_str());*/
    V2RFluormeterdata_pub_.publish(msg);
  } else {
    ROS_INFO("msg #F is inactive");
  }
}

void MessageParser::V2RVersionInformationCallback(
    const ros::Time& msg_time, std::vector<std::string> token_vec) {
  if (V_active) {
    remus_backseat_msgs::V2RVersionInformation msg;
    msg.header.stamp = msg_time;
    /*msg.version =
backseatUtility::naive(backseatUtility::parseString(stringToParse,"","").c_str());
    msg.monthAndDay =
backseatUtility::parseString(stringToParse," "," "); msg.year
= atoi(backseatUtility::parseString(stringToParse," "," ").c_str());
//break apart the time coming in
    msg.time =
backseatUtility::parseString(stringToParse,"","*");
    //std:: string strTime = currentDelimPull;
    //delimStringSetToken(":","");
    //int hour =atoi(currentDelimPull.c_str());
    //delimStringSetToken(":","");
    //int min = atoi(currentDelimPull.c_str());
    //delimStringSetToken(":","*");
    //int sec = atoi(currentDelimPull.c_str());
    //msg.time = ros::Time::now(hour,min, sec);*/

    // V2RVersioninformation_pub_.publish(msg);
  } else {
    ROS_INFO("msg #V is inactive");
  }
}

// This one is weird, because there will be a series of #R messages
// one for each leg. The final transmission will be blank (e.g. "#R,5,*2C")
// The current implemtation is buggy -- route message needs to be a member
// variable, and should be cleared whenever leg = 0, and published whenever
// a leg has no additional fields.
void MessageParser::V2RRouteCallback(const ros::Time& msg_time,
                                     std::vector<std::string> token_vec) {
  if (R_active) {
    remus_backseat_msgs::V2RRoute msg;
    msg.header.stamp = msg_time;
    /*int legNum = 0;
    legNum = atoi(backseatUtility::parseString(stringToParse,"","").c_str());
    msg.legNumber.push_back(legNum);
    std::string compare = backseatUtility::parseString(stringToParse,"","");
    if(compare == "*") { //case that no nav msg
            ROS_INFO("INR");
            V2RRouteinformation_pub_.publish(msg);
    } else {

            msg.objective.push_back(compare);
            msg.startPos.push_back(backseatUtility::parseString(stringToParse,"",""));
            msg.endPos.push_back(backseatUtility::parseString(stringToParse,"",""));
            msg.altitude.push_back(backseatUtility::parseString(stringToParse,"",""));
            msg.speedRPM.push_back(backseatUtility::parseString(stringToParse,"",""));
    }*/
  } else {
    ROS_INFO("msg #R is inactive");
  }
}

void MessageParser::V2RTimeCallback(const ros::Time& msg_time,
                                    std::vector<std::string> token_vec) {
  if (Z_active) {
    remus_backseat_msgs::V2RTime msg;
    msg.header.stamp = msg_time;
    msg.date_string = token_vec[1];
    msg.time_string = token_vec[2];
    struct tm timeinfo;
    timeinfo.tm_year = atoi(token_vec[1].substr(7).c_str()) - 1900;
    timeinfo.tm_mon = atoi(token_vec[1].substr(1, 2).c_str()) - 1;
    timeinfo.tm_mday = atoi(token_vec[1].substr(4, 2).c_str());
    timeinfo.tm_hour = atoi(token_vec[2].substr(1, 2).c_str());
    timeinfo.tm_min = atoi(token_vec[2].substr(4, 2).c_str());
    timeinfo.tm_sec = atoi(token_vec[2].substr(7, 2).c_str());
    msg.unixtime = mktime(&timeinfo);
    V2RTimemessage_pub_.publish(msg);
  } else {
    ROS_INFO("msg #Z is inactive");
  }
}

void MessageParser::V2RModemDirectMonitoringInterfaceCallback(
    const ros::Time& msg_time, std::vector<std::string> token_vec) {
  if (M_active) {
    remus_backseat_msgs::V2RModemDirectMonitoringInterface msg;
    msg.header.stamp = msg_time;
    /*std:: string msgType = backseatUtility::parseString(stringToParse,"","");
    msg.msgData = msgType;
    if(msgType ==  "MSG_DATA") {
            msg.source =
    backseatUtility::parseString(stringToParse,"","");
            msg.dest =
    backseatUtility::parseString(stringToParse,"","");
            msg.ack =
    backseatUtility::parseString(stringToParse,"","");
            msg.framenum =
    backseatUtility::parseString(stringToParse,"","");
            msg.hexdata =
    backseatUtility::parseString(stringToParse,"",""); } else if(msgType ==
    "MSG_OWTT") { msg.source =
    backseatUtility::parseString(stringToParse,"","");
            msg.dest =
    backseatUtility::parseString(stringToParse,"","");
            msg.traveltime =
    backseatUtility::parseString(stringToParse,"","");
            msg.rangemeters =
    backseatUtility::parseString(stringToParse,"",""); } else if(msgType ==
    "MSG_ACK") { msg.source =
    backseatUtility::parseString(stringToParse,"","");
            msg.dest =
    backseatUtility::parseString(stringToParse,"","");
            msg.framenum =
    backseatUtility::parseString(stringToParse,"",""); } else if(msgType ==
    "MSG_DETECT") { msg.mfdpeak =
    backseatUtility::parseString(stringToParse,"","");
            msg.power =
    backseatUtility::parseString(stringToParse,"","");
            msg.ratio =
    backseatUtility::parseString(stringToParse,"","");
            msg.spl =
    backseatUtility::parseString(stringToParse,"",""); } else if(msgType ==
    "MSG_QUALITY") { msg.qualityfactor =
    backseatUtility::parseString(stringToParse,"","");
            msg.packettype =
    backseatUtility::parseString(stringToParse,"","");
    }*/
    V2RModemdirectmonitoringinterface_pub_.publish(msg);
  } else {
    ROS_INFO("msg #M is inactive");
  }
}

void MessageParser::V2RSidescanNotificationCallback(
    const ros::Time& msg_time, std::vector<std::string> token_vec) {
  if (N_active) {
    remus_backseat_msgs::V2RSidescanNotification msg;
    msg.header.stamp = msg_time;
    /*msg.sideScan =
     * backseatUtility::parseString(stringToParse,"","");*/
    V2RSidescannotifymessage_pub_.publish(msg);
  } else {
    ROS_INFO("msg #N is inactive");
  }
}

void MessageParser::V2RDusblRangeBearingCallback(
    const ros::Time& msg_time, std::vector<std::string> token_vec) {
  if (U_active) {
    remus_backseat_msgs::V2RDusblRangeBearing msg;
    msg.header.stamp = msg_time;
    /*msg.range =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"","").c_str());
    msg.bearing =
    backseatUtility::naive(backseatUtility::parseString(stringToParse,"","").c_str());*/
    V2RDUSBLrangebearing_pub_.publish(msg);
  } else {
    ROS_INFO("msg #U is inactive");
  }
}

void MessageParser::V2RErrorCallback(const ros::Time& msg_time,
                                     std::vector<std::string> token_vec) {
  if (E_active) {
    remus_backseat_msgs::V2RError msg;
    msg.header.stamp = msg_time;
    /*msg.errorType =
    atoi(backseatUtility::parseString(stringToParse,"","*").c_str());
    msg.asciiMsg =
    backseatUtility::parseString(stringToParse,"","*");*/
    V2RErrormessage_pub_.publish(msg);
  } else {
    ROS_INFO("msg #E is inactive");
  }
}
