## remus_recon_interface

These packages were written by Grek Okopal, and were part of his backseat
repository for remus control:
* https://bitbucket.org/gokopal/backseat
* https://gitlab.com/apl-ocean-engineering/mcneil-auv-lab/greg_backseat

However, they're useful even in cases where we aren't using the full backseat
control stack, so I've pulled them into a separate repository so compilation on
the ODroid won't take quite as long. See the origianl backseat repo for
additional documentation / examples as part of a larger system.

Included repositories:
* msg_composer: subscribes to messages from the backseat and composes the corresponding Recon string
* msg_parser: subscribes to string coming from Recon, and parses it into a ROS message
* frontseat_interface: contains 2 nodes that handle the UDP interface with Recon, providing a ROS message interface using string messages.
