/**
 * Copyright 2018-2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

//
//  MessageComposer.cpp
//
//
//  Created by Greg on 11/2/18.
//

#include "msg_composer/MessageComposer.h"

void MessageComposer::AddChecksum(std::string* input_string) {
  int sum = 0;
  input_string->append(",*");
  for (unsigned int ii = 0; ii < input_string->length(); ii++) {
    sum += input_string->at(ii);
  }
  std::stringstream hexstream;
  hexstream << std::uppercase << std::setfill('0') << std::setw(2) << std::hex
            << (sum & (0x0FF));
  input_string->append(hexstream.str() + "\n");
}

void MessageComposer::r2VHeartbeatRequestCallback(
    const remus_backseat_msgs::R2VHeartbeatRequest::ConstPtr& in_msg) {
  time_t t = time(0);  // get time now

  // construct a heartbeat response and send it
  std_msgs::String out_msg;
  out_msg.data = std::string("#V,") + boost::lexical_cast<std::string>(t);
  AddChecksum(&out_msg.data);
  frontseat_pub_.publish(out_msg);
}

void MessageComposer::r2VSetDestinationCallback(
    const remus_backseat_msgs::R2VSetDestination::ConstPtr& in_msg) {
  std_msgs::String out_msg;
  double latitude_degrees, latitude_minutes, longitude_degrees,
      longitude_minutes;
  latitude_minutes = fabs(modf(in_msg->latitude, &latitude_degrees));
  latitude_minutes = latitude_minutes * 60.0;
  longitude_minutes = fabs(modf(in_msg->longitude, &longitude_degrees));
  longitude_minutes = longitude_minutes * 60.0;
  std::string NS_str, EW_str;
  if (in_msg->latitude > 0) {
    NS_str = std::string("N");
  } else {
    NS_str = std::string("S");
  }
  if (in_msg->longitude > 0) {
    EW_str = std::string("E");
  } else {
    EW_str = std::string("W");
  }

  // this makes sure the lat/longs are formatted to 4 decimal points
  int latitude_int_degrees = static_cast<int>(latitude_degrees);
  std::string latitude_str =
      boost::lexical_cast<std::string>(latitude_int_degrees) + NS_str;
  std::stringstream latitude_minutes_stream;
  latitude_minutes_stream << std::fixed << std::setprecision(4)
                          << latitude_minutes;
  int longitude_int_degrees = static_cast<int>(longitude_degrees);
  std::string longitude_str =
      boost::lexical_cast<std::string>(longitude_int_degrees) + EW_str;
  std::stringstream longitude_minutes_stream;
  longitude_minutes_stream << std::fixed << std::setprecision(4)
                           << longitude_minutes;
  out_msg.data = std::string("#C,Heading,Destination,") + latitude_str +
                 latitude_minutes_stream.str() + " " + longitude_str +
                 longitude_minutes_stream.str();
  AddChecksum(&out_msg.data);
  frontseat_pub_.publish(out_msg);
}

void MessageComposer::r2VEnableFullOverrideCallback(
    const remus_backseat_msgs::R2VEnableFullOverride::ConstPtr& in_msg) {
  std_msgs::String out_msg;  // fix this
  if (in_msg->enable) {
    out_msg.data = std::string("#E,1");
  } else {
    out_msg.data = std::string("#E,0");
  }
  AddChecksum(&out_msg.data);
  frontseat_pub_.publish(out_msg);
}

void MessageComposer::r2VSetDepthCallback(
    const remus_backseat_msgs::R2VSetDepth::ConstPtr& in_msg) {
  std_msgs::String out_msg;
  out_msg.data = std::string("#C,Depth,Depth,") +
                 boost::lexical_cast<std::string>(in_msg->depth);
  AddChecksum(&out_msg.data);
  frontseat_pub_.publish(out_msg);
}

void MessageComposer::r2VSetAltitudeCallback(
    const remus_backseat_msgs::R2VSetAltitude::ConstPtr& in_msg) {
  std_msgs::String out_msg;
  out_msg.data = std::string("#C,Depth,Altitude,") +
                 boost::lexical_cast<std::string>(in_msg->altitude);
  AddChecksum(&out_msg.data);
  frontseat_pub_.publish(out_msg);
}

void MessageComposer::r2VSetSpeedCallback(
    const remus_backseat_msgs::R2VSetSpeed::ConstPtr& in_msg) {
  std_msgs::String out_msg;
  std::string unitstring;
  if (in_msg->units == "knots") {
    unitstring = std::string(" knots");
  } else if (in_msg->units == "rpm") {
    unitstring = std::string(" rpm");
  } else {
    unitstring = std::string(" meters/sec");
  }
  std::stringstream speed_stream;
  speed_stream << std::fixed << std::setprecision(1) << in_msg->speed;
  out_msg.data = std::string("#C,Speed,") + speed_stream.str() + unitstring;
  AddChecksum(&out_msg.data);
  frontseat_pub_.publish(out_msg);
}

void MessageComposer::r2VSetHeadingCallback(
    const remus_backseat_msgs::R2VSetHeading::ConstPtr& in_msg) {
  std_msgs::String out_msg;
  std::stringstream heading_stream;
  heading_stream << std::fixed << std::setprecision(1) << in_msg->heading;
  out_msg.data = std::string("#C,Heading,Goal,") + heading_stream.str();
  AddChecksum(&out_msg.data);
  frontseat_pub_.publish(out_msg);
}

void MessageComposer::r2VModemMessageCallback(
    const remus_backseat_msgs::R2VModemMessage::ConstPtr& in_msg) {
  std_msgs::String out_msg;
  std::string reqAck, overwrite;
  if (in_msg->requestAck) {
    reqAck = "1,";
  } else {
    reqAck = "0,";
  }

  if (in_msg->overwriteExisting) {
    overwrite = "1,";
  } else {
    overwrite = "0,";
  }

  // convert input string to hex string
  std::string hexstring;
  std::stringstream hexchar;
  // hexstring.reserve(2*in_msg->message.length());
  std::string instring = in_msg->message;
  for (char& c : instring) {
    hexchar << std::hex << static_cast<int>(c);
  }
  out_msg.data = std::string("#m,") + reqAck + overwrite + hexchar.str();
  AddChecksum(&out_msg.data);
  frontseat_pub_.publish(out_msg);
}

void MessageComposer::r2VAbortMissionCallback(
    const remus_backseat_msgs::R2VAbortMission::ConstPtr& in_msg) {
  std_msgs::String out_msg;
  out_msg.data = "#E,-1";
  AddChecksum(&out_msg.data);
  frontseat_pub_.publish(out_msg);
}

MessageComposer::MessageComposer() {
  nh_ = ros::NodeHandle();
  // publishes the string to recon_interface_outgoing
  frontseat_pub_ =
      nh_.advertise<std_msgs::String>("composer_string_to_frontseat", 1000);

  // subscribe to the ROS messages that will be translated to strings
  abort_mission_sub_ = nh_.subscribe(
      "R2VAbortMission", 1000, &MessageComposer::r2VAbortMissionCallback, this);
  enable_full_override_sub_ =
      nh_.subscribe("R2VEnableFullOverride", 1000,
                    &MessageComposer::r2VEnableFullOverrideCallback, this);
  heartbeat_request_sub_ =
      nh_.subscribe("R2VHeartbeatRequest", 1000,
                    &MessageComposer::r2VHeartbeatRequestCallback, this);
  modem_message_sub_ = nh_.subscribe(
      "R2VModemMessage", 1000, &MessageComposer::r2VModemMessageCallback, this);
  set_altitude_sub_ = nh_.subscribe(
      "R2VSetAltitude", 1000, &MessageComposer::r2VSetAltitudeCallback, this);
  set_depth_sub_ = nh_.subscribe("R2VSetDepth", 1000,
                                 &MessageComposer::r2VSetDepthCallback, this);
  set_destination_sub_ =
      nh_.subscribe("R2VSetDestination", 1000,
                    &MessageComposer::r2VSetDestinationCallback, this);
  set_speed_sub_ = nh_.subscribe("R2VSetSpeed", 1000,
                                 &MessageComposer::r2VSetSpeedCallback, this);
  set_heading_sub_ = nh_.subscribe(
      "R2VSetHeading", 1000, &MessageComposer::r2VSetHeadingCallback, this);
}
