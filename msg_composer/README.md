## msg_composer

Subscribes to ROS messages and creates the corresponding std_msgs::string
output that will be sent to Recon via the frontseat_interface.

TODO: add R2VAbort message that sends "E,-1" (after testing that it works as expected on the bench)

Supported messages:
* #C -- (all control inputs) R2VSetDepth, R2VSetAltitude, R2VSetSpeed, R2VSetHeading, R2VSetDestination
* #E -- R2VEnableFullOverride (but only supports "#E,1" and "#E,0"; we need "#E,-1" for abort)
* #V -- R2VHeartbeatRequest "transmit version information to vehicle"

* #m -- R2VModemMessage: "send user data via acoustic modem"


List of all remote-to-vehicle (R2V) messages not (yet?) supported:

* #D -- Disable : "enable/disable depth-only override"
* #H -- "provide control commands for the hovering module"
* #L -- "Send a CCL command directly to the vehicle to execute"
* #M -- "Modem direct command interface
* #N -- Nav reset: "Sets the position of the vehicle"
* #P -- "add/set payload status"
* #Q -- query heading/speed/depth
* #R -- Request "request route information from the vehicle"
* #T -- "Change transponder position"

* #a -- "anchoring commands to the vehicle"
* #c -- set closest: "set return to closest point on track line after override"
* #p -- "send acoustic modem command to the modem"
* #r -- "send a special ranger message"
* #s -- set : "sets a parameter"
* #t -- "terminates the current module and starts the next"
* #v -- request version information
