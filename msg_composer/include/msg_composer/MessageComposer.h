/**
 * Copyright 2018-2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
//  MessageComposer.h
//
//
//  Created by Greg on 11/2/18.
//

#ifndef MSG_COMPOSER_INCLUDE_MSG_COMPOSER_MESSAGECOMPOSER_H_
#define MSG_COMPOSER_INCLUDE_MSG_COMPOSER_MESSAGECOMPOSER_H_

#include <iostream>
#include <sstream>
#include <string>

#include "remus_backseat_msgs/R2VAbortMission.h"
#include "remus_backseat_msgs/R2VEnableFullOverride.h"
#include "remus_backseat_msgs/R2VHeartbeatRequest.h"
#include "remus_backseat_msgs/R2VModemMessage.h"
#include "remus_backseat_msgs/R2VSetAltitude.h"
#include "remus_backseat_msgs/R2VSetDepth.h"
#include "remus_backseat_msgs/R2VSetDestination.h"
#include "remus_backseat_msgs/R2VSetHeading.h"
#include "remus_backseat_msgs/R2VSetSpeed.h"
#include "ros/ros.h"
#include "std_msgs/String.h"

class MessageComposer {
 public:
  MessageComposer();
  ~MessageComposer() = default;

 private:
  // Calculate and append checksum to input string
  void AddChecksum(std::string* input_string);

  // Callbacks that turn a ROS message into a std_msgs::String formatted
  // as Recon expects.
  void r2VAbortMissionCallback(
      const remus_backseat_msgs::R2VAbortMission::ConstPtr& in_msg);
  void r2VHeartbeatRequestCallback(
      const remus_backseat_msgs::R2VHeartbeatRequest::ConstPtr& in_msg);
  void r2VSetDestinationCallback(
      const remus_backseat_msgs::R2VSetDestination::ConstPtr& in_msg);
  void r2VEnableFullOverrideCallback(
      const remus_backseat_msgs::R2VEnableFullOverride::ConstPtr& in_msg);
  void r2VSetDepthCallback(
      const remus_backseat_msgs::R2VSetDepth::ConstPtr& in_msg);
  void r2VSetAltitudeCallback(
      const remus_backseat_msgs::R2VSetAltitude::ConstPtr& in_msg);
  void r2VSetSpeedCallback(
      const remus_backseat_msgs::R2VSetSpeed::ConstPtr& in_msg);
  void r2VModemMessageCallback(
      const remus_backseat_msgs::R2VModemMessage::ConstPtr& in_msg);
  void r2VSetHeadingCallback(
      const remus_backseat_msgs::R2VSetHeading::ConstPtr& in_msg);

  ros::NodeHandle nh_;

  ros::Publisher frontseat_pub_;

  ros::Subscriber abort_mission_sub_;
  ros::Subscriber enable_full_override_sub_;
  ros::Subscriber heartbeat_request_sub_;
  ros::Subscriber modem_message_sub_;
  ros::Subscriber set_altitude_sub_;
  ros::Subscriber set_depth_sub_;
  ros::Subscriber set_destination_sub_;
  ros::Subscriber set_heading_sub_;
  ros::Subscriber set_speed_sub_;
};

#endif  // MSG_COMPOSER_INCLUDE_MSG_COMPOSER_MESSAGECOMPOSER_H_
